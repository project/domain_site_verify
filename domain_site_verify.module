<?php

/**
 * @file
 * Logic to add domain module integration for site_verify.
 */

/**
 * Implements hook_menu_alter().
 *
 * For all file verifications, update the page callback.
 *
 * This ensures the site_verify_output() wrapper function,
 * domain_site_verify_output() gets called.
 */
function domain_site_verify_menu_alter(&$items) {
  $verifications = db_query("SELECT svid, file FROM {site_verify} WHERE file <> ''")->fetchAll();
  foreach ($verifications as $verification) {
    if (isset($items[$verification->file])) {
      $items[$verification->file]['page callback'] = 'domain_site_verify_output';
    }
  }

  /*
   * In order to use our custom loader function, we have to change the menu key
   * for site verification CRUD op forms.
   */
  $sv_edit = 'admin/config/search/verifications/%site_verify/edit';
  $sv_delete = 'admin/config/search/verifications/%site_verify/delete';
  if (isset($items[$sv_edit]) && isset($items[$sv_delete])) {
    domain_site_verify_array_key_replace($items, $sv_edit, 'admin/config/search/verifications/%domain_site_verify/edit');
    domain_site_verify_array_key_replace($items, $sv_delete, 'admin/config/search/verifications/%domain_site_verify/delete');
  }
}

/**
 * Implements hook_html_head_alter().
 *
 * Remove any verification tags not specified for the current domain.
 */
function domain_site_verify_html_head_alter(&$head_elements) {
  foreach ($head_elements as $key => $value) {
    if (preg_match('/site_verify:\d+/', $key)) {
      if (!domain_site_verify_domain(substr($key, strpos($key, ':') + 1))) {
        unset($head_elements[$key]);
      }
    }
  }
}

/**
 * Implements hook_form_BASE_ID_alter().
 *
 * Add a select box to choose an active domain on the site verification form.
 */
function domain_site_verify_form_site_verify_edit_form_alter(&$form, &$form_state) {
  if ($form_state['storage']['step'] == 2) {
    $domains = domain_domains();

    $options = array();
    foreach ($domains as $domain) {
      $options[$domain['domain_id']] = $domain['sitename'];
    }

    $domain_id = ($domain_id = $form_state['storage']['record']['domain_id']) ? $domain_id : FALSE;

    $form['domain_id'] = array(
      '#type' => 'select',
      '#title' => 'Active Domain',
      '#options' => $options,
      '#default_value' => ($domain_id) ? $domain_id : t('Choose domain'),
      '#description' => t('Choose the domain for which this verification should be active.'),
    );
  }
}

/**
 * Menu load callback; loads a site verification record.
 *
 * This is nearly a complete duplicate of site_verify_load(), it only changes
 * the field query to use an asterisk for its fields and creates a static. This
 * also loads the engine details if the record was found.
 *
 * @deprecation This function will be removed once/if patch to site_verify is
 *   accepted.
 *
 * @param $svid
 *   A site verification ID.
 * @return
 *   An array of the site verification record, or FALSE if not found.
 */
function domain_site_verify_load($svid) {
  $record = &drupal_static(__FUNCTION__ . $svid);
  if (!$record) {
    $record = db_query("SELECT * FROM {site_verify} WHERE svid = :svid", array(':svid' => $svid))->fetchAssoc();
    if ($record) {
      $record['engine'] = site_verify_engine_load($record['engine']);
    }
  }
  return $record;
}

/**
 * Wrapper function for site_verify_output().
 *
 * Check that there exists a verification with svid on the current domain
 * before passing the request through to site_verify_output().
 */
function domain_site_verify_output($svid) {
  $verification = domain_site_verify_load($svid);
  // If a result was found, pass on through to site_verify_output().
  if (domain_site_verify_domain($verification)) {
    if ($verification['file_contents'] && $verification['engine']['file_contents']) {
      echo $verification['file_contents'];
    }
    else {
      drupal_set_title(t('Verification page'));
      return t('This is a verification page for the @title search engine.', array('@title' => $verification['engine']['name']));
    }
  }
  else {
    drupal_fast_404();
    return FALSE;
  }
}

/**
 * Helper function to verify that a given verification is active for the current domain.
 *
 * @param mixed $verification
 *   A site_verify verification with domain_id or an svid.
 *
 * @return bool
 *   Returns TRUE if the verification belongs to the current domain, FALSE
 *   otherwise.
 */
function domain_site_verify_domain($verification) {
  if (!is_array($verification)) {
    $verification = domain_site_verify_load($verification);
  }
  // Get current domain.
  $domain = domain_get_domain();
  return ($verification['domain_id'] == $domain['domain_id']);
}

/**
 * Replaces array key, $original, with $replace.
 *
 * Throws exception if $replace already exists. Does NOT preserve order.
 *
 * @param array $array
 *   Array to be modified.
 * @param string $original
 *   Original array key.
 * @param string $replace
 *   Replacement array key.
 * @throws Exception
 */
function domain_site_verify_array_key_replace(&$array, $original, $replace) {
  if (!isset($array[$replace])) {
    $array[$replace] = $array[$original];
    unset($array[$original]);
  }
  else {
    throw new Exception('Cannot replace key, key already exists.');
  }
}
